<?php
/// Location of debian server:
$debserver="http://cern.ch/lcg-heppkg/debian/";

$dist=$_GET['dist'];
$arch=$_GET['arch'];

if ( $dist == "" ) {
    $dist="sid";
}
if ( $arch == "" ) {
    $arch="amd64";
}

function links($myarr,$myvar,$othstr,$initstr) {
    $fname="index.php";
    echo $initstr;
    for ( $i = 0; $i < count($myarr); $i += 1) {
        echo '<a href="'.$fname.'?'.$othstr.'&'.$myvar.'='.$myarr[$i].'">'.$myarr[$i].'</a> ';
    }
    echo "<br>";
}

$initstr="Choose your distribution: ";
$myarr=array('intrepid','lenny','sid','squeeze'); 
$myvar="dist";
$othstr="arch=".$arch;
links($myarr,$myvar,$othstr,$initstr);
$initstr="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$myarr=array('oldstable', 'stable', 'unstable', 'testing');
links($myarr,$myvar,$othstr,$initstr);
$initstr="Choose your architecture: ";
$myarr=array('i386','amd64', 'ia64', 'powerpc');
$myvar="arch";
$othstr="dist=".$dist;
links($myarr,$myvar,$othstr,$initstr);

echo "<br><br>";

$pkgfile=$debserver."dists/".$dist."/hep/binary-".$arch."/Packages";
$fh = fopen($pkgfile, 'r');
$b1 = "<tr><td><b>";
$b2 = ":</b></td><td> ";
$b3 = "</td><td>";
$b4 = "</td></tr>";
echo "<u>Available packages for Debian <b>".$dist."</b> on architecture <b>".$arch."</b></u>:<br><br><table>";
echo "<tr><th>Package Name</th><th>Version</th><th>Description</th></tr>";
while ( $mline=fgets($fh) ) {
    $linearr=preg_split('/\s+/',$mline);
    if ( $linearr[0] == "Package:" ) {
        echo $b1.$linearr[1].$b2;
    }
    if ( $linearr[0] == "Version:" ) {
        $narr= preg_split('/\: /',$mline);
        echo $narr[1].$b3;
    }
    if ( $linearr[0] == "Description:" ) {
        $narr= preg_split('/\: /',$mline);
        echo $narr[1].$b4;
    }
}
echo "</table>";

?>
